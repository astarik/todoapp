//
//  ViewController.m
//  toDoApp
//
//  Created by admin on 10/16/16.
//  Copyright © 2016 staryaprod. All rights reserved.
//

#import "ViewController.h"
#import "Task.h"

@interface ViewController () <UITextFieldDelegate>

@property (nonatomic, assign) BOOL isEditMode;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (!self.task) {
        self.task = [[Task alloc] init];
        self.isEditMode = NO;
    } else {
        self.isEditMode = YES;
    }
    self.titleField.delegate = self;
    self.descriptionField.delegate = self;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldDidChanged:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:nil];
    [self.saveButton setEnabled:false];
    
    if (_isEditMode) {
        self.titleField.text = self.task.title;
        self.descriptionField.text = self.task.details;
    }
}

- (void)textFieldDidChanged:(NSNotification *)notification {
    UITextField *editingTextField = (UITextField *)notification.object;
    if (editingTextField.text.length > 0) {
        [self.saveButton setEnabled:true];
    } else {
        [self.saveButton setEnabled:false];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (self.saveButton.enabled == true) {
        [textField resignFirstResponder];
        [self save:_saveButton];
    }
    return YES;
}

- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
    [super.navigationController popViewControllerAnimated:YES];
}

- (IBAction)save:(id)sender {
    if ([self.titleField.text  isEqual: @""] || self.titleField.text.length == 0) {
        self.task.title = self.descriptionField.text;
        self.task.details = nil;
    } else {
        self.task.title = self.titleField.text;
        self.task.details = self.descriptionField.text;
    }
    self.task.date = [NSDate date];
    
    if (_isEditMode) {
        [self.delegate didEditTodo:self.task];
    } else {
        [self.delegate didSaveNewTodo:self.task];
    }
    [self dismissViewControllerAnimated:true completion:nil];
    [super.navigationController popViewControllerAnimated:YES];
}
@end
