//
//  Task.h
//  toDoApp
//
//  Created by admin on 11/6/16.
//  Copyright © 2016 staryaprod. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Task : NSObject <NSCoding>

@property (nonatomic, retain) NSString* title;
@property (nonatomic, retain) NSString* details;
@property (nonatomic, retain) NSDate* date;

@end
