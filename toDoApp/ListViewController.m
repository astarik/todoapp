//
//  ListViewController.m
//  toDoApp
//
//  Created by admin on 10/16/16.
//  Copyright © 2016 staryaprod. All rights reserved.
//

#import "ListViewController.h"
#import "ViewController.h"
#import "Task.h"

@interface ListViewController () <AddItemControllerDelegate>

@property (nonatomic, strong) NSString* filePath;

@end

@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.taskLsit = [self getTasksFromPlist];
}

# pragma mark - Custom functions

- (void)didSaveNewTodo:(Task *) task {
    [self.taskLsit addObject:task];
    [self.tableView reloadData];
    [self saveTasksToPlist: self.taskLsit];
}

- (void) didEditTodo:(Task *) task {
    NSUInteger index =  [self.taskLsit indexOfObject:task];
    [self.taskLsit replaceObjectAtIndex:index withObject:task];
    [self.tableView reloadData];
    [self saveTasksToPlist: self.taskLsit];
}

- (NSMutableArray*) getTasksFromPlist {
    
    NSError* error;
    NSArray* pListpaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString* pListdocumentsDirectory = [pListpaths objectAtIndex:0];
    _filePath = [pListdocumentsDirectory stringByAppendingPathComponent:@"Tasks.plist"];
    
    NSFileManager* fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath: _filePath])    {
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"Tasks" ofType:@"plist"];
        [fileManager copyItemAtPath:bundle toPath: _filePath error:&error];
    }
    
    NSMutableArray* tasks = [[NSMutableArray alloc] init];
    
    NSArray *savedStock = [[NSArray alloc] initWithContentsOfFile: _filePath];
    if (savedStock.count > 0) {
        for (int i = 0; i < savedStock.count; i++) {
            Task* retrievedTask = [NSKeyedUnarchiver unarchiveObjectWithData:savedStock[i]];
            [tasks addObject:retrievedTask];
        }
    } else {
        tasks = [[NSMutableArray alloc] init];
    }
    return tasks;
}

- (void) saveTasksToPlist:(NSMutableArray*) tasks {
    NSMutableArray* tasksToSave = [[NSMutableArray alloc] init];
    for (Task* task in tasks) {
        NSData* retrievedTask = [NSKeyedArchiver archivedDataWithRootObject:task];
        [tasksToSave addObject:retrievedTask];
    }
    [tasksToSave writeToFile:_filePath atomically:YES];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.taskLsit.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    Task* task = self.taskLsit[indexPath.row];
    cell.textLabel.text = task.title;
    cell.detailTextLabel.text = task.details;
    [cell.detailTextLabel setUserInteractionEnabled:NO];
    [cell.textLabel setUserInteractionEnabled:NO];
    return cell;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    UINavigationController* nav = segue.destinationViewController;
    ViewController * addVC = nav.viewControllers[0];
    addVC.delegate = self;
}

 //Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
     //Return NO if you do not want the specified item to be editable.
    return YES;
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    Task* task = self.taskLsit[indexPath.row];
    UITableViewRowAction *editAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Edit" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
            // here goes another view to proceed with edidting.
        ViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
        controller.delegate = self;
        controller.task = task;
        [self.navigationController pushViewController:controller animated:YES];

    }];
    
    editAction.backgroundColor = [UIColor grayColor];
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Delete"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        [self.taskLsit removeObject: task];
        [self saveTasksToPlist: self.taskLsit];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }];
    deleteAction.backgroundColor = [UIColor redColor];
    return @[deleteAction,editAction];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    CGFloat hue = ( arc4random() % 256 / 256.0 );
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;
    UIColor *color = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];

    cell.backgroundColor = color;
    
    return indexPath;
}

@end
