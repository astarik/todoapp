//
//  ViewController.h
//  toDoApp
//
//  Created by admin on 10/16/16.
//  Copyright © 2016 staryaprod. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Task.h"

@protocol AddItemControllerDelegate

-(void) didSaveNewTodo:(Task* ) task;
-(void) didEditTodo:(Task* ) task;

@end


@interface ViewController : UIViewController
- (IBAction)cancel:(id)sender;
- (IBAction)save:(id)sender;
@property (nonatomic, strong) Task* task;
@property (strong, nonatomic) id <AddItemControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;
@property (weak, nonatomic) IBOutlet UITextField *titleField;
@property (weak, nonatomic) IBOutlet UITextField *descriptionField;

@end

