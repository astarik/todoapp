//
//  Task.m
//  toDoApp
//
//  Created by admin on 11/6/16.
//  Copyright © 2016 staryaprod. All rights reserved.
//

#import "Task.h"

@implementation Task

- (id)init {
    self = [super init];
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    if (self = [super init]) {
        self.title = [decoder decodeObjectForKey:@"title"];
        self.details = [decoder decodeObjectForKey:@"details"];
        self.date = [decoder decodeObjectForKey:@"date"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:_title forKey:@"title"];
    [encoder encodeObject:_details forKey:@"details"];
    [encoder encodeObject:_date forKey:@"date"];
}

@end
